<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'newfile1' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'Ya)<39Ku.5f;.h5>@]sKFj >PWr7NvXP]`RMsoc[zCe(IiM:K8m8m{aRw ;f{pGm' );
define( 'SECURE_AUTH_KEY',  'ZVERE({n9yl!{dEZZEUaD+G#gTi:@T7Lw?bspJ5(dh6#jAaLUn^5y=|ZyD84Z ;X' );
define( 'LOGGED_IN_KEY',    'v>Sb0&]?;^|Le?87^iY5Y%~gwUHYysNvf=4%[&?,=r1Q<;3=6`,w)%UkT@_:rt~A' );
define( 'NONCE_KEY',        'H&w!Aj AoTi?Nj _/5l+kDYl.3%O~3S=X]U6>.}C!c8qYt%-]S%pO{x2YQ?5,+DF' );
define( 'AUTH_SALT',        'O,];D![sQqr,XO/K5Gl[=@S&QN]4Mo/4oI(Wk+MA$kRgl*1.j#xfsZe?j>tzCZf?' );
define( 'SECURE_AUTH_SALT', '>X#|A=)E=COFm+3F*f;kRJEpR$h%3:OfhqeOcz5_]!ZIO]LWp0c_JpU,A!w,_!/X' );
define( 'LOGGED_IN_SALT',   '{5hzFP7pfu@MW+R%{~UrW|7 ~kKbr`:;;~J^ jiJuVTxb24o56PH((1 yq3qH}WQ' );
define( 'NONCE_SALT',       '#9|s18vnW~%a~&7|: UrK5.c?zT;r.b{9B}TYTUPI(0{j>Df5:>^ajW,37FUE4kc' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
