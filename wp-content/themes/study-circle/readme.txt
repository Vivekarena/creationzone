=== Study Circle ===
Contributors: Grace Themes
Tags:blog,two-columns,left-sidebar,full-width-template,custom-colors,custom-menu,custom-header,custom-logo,featured-images,editor-style,custom-background,threaded-comments,theme-options, translation-ready

Requires at least: 4.0
Tested up to: 4.9.8
Version: 1.8.0
License: GNU General Public License version 2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Description ==
Study Circle is a free Education WordPress theme. It is perfect for school, college, tution classes, coaching classes, personal, bloging and any small business. It is user friendly customizer options and Compatible in wordPress Latest Version. also Compatible with WooCommerce, Nextgen gallery ,Contact Form 7 and many WordPress popular plugins.

== Theme License & Copyright ==
* Study Circle WordPress Theme, Copyright 2018 Grace Themes
* Study Circle is distributed under the terms of the GNU GPL

== Changelog ==

Version 1.0.0
i) Intial version Release

Version 1.1.0
i) Updated the theme as per new guideline.

Version 1.2.0
i) fixed remaining minor issues as per new guideline.

Version 1.3.0
i) Solved minor issues as per new guideline.

Version 1.4.0
i) Solved some minor issues.

Version 1.5.0
i) fixed all issues as per reviewer pointed out.

Version 1.6.0
i) fixed all issues as per reviewer pointed.

Version 1.7.0
i) prefix error solved as per reviewer pointed out.

Version 1.8.0
i) Updated the theme

== Resources ==

Theme is Built using the following resource bundles.

1 - All js that have been used are within folder /js of theme.
2 - jQuery Nivo Slider
	Copyright 2012, Dev7studios, under MIT license
	http://nivo.dev7studios.com

3 - Montserrat :https://www.google.com/fonts/specimen/Montserrat
	License: Distributed under the terms of the Apache License, version 2.0 		
	http://www.apache.org/licenses/LICENSE-2.0.html

4 - Images used from Pixabay.
    Pixabay provides images under CC0 license 
   (https://creativecommons.org/about/cc0)

	Slider Images:	
	https://pixabay.com/en/language-school-team-interns-834138/

	Other Images:
	https://pixabay.com/en/class-discussion-girls-study-child-302116/
	https://pixabay.com/en/read-book-girl-study-learn-sofa-515531/
	https://pixabay.com/en/birger-kollmeier-professor-910261/
	
5 - Font-Awesome
	Font Awesome 4.5.0 by @davegandy - http://fontawesome.io - @fontawesome
 	License - http://fontawesome.io/license (Font: SIL OFL 1.1, CSS: MIT License)
	

For any help you can mail us at support[at]gracethemes.com